import sys
import argparse

from register import Register

def startup():
	print(''.ljust(47, '-'))
	print('Thank you for using the lunar changemaker 5000!')
	print(''.rjust(47, '-'), '\n')

	parser = argparse.ArgumentParser(description='Calculate precise denominations of change to give')

	parser.add_argument('total_cost', type=float,
	                   help='float representing the total cost of the purchase')

	parser.add_argument('amount_provided', type=float,
	                   help='float representing the USD amount tendered by the buyer')

	# a flag to show bills remaining in the register at the end of output
	parser.add_argument('--show-remaining', action='store_true',
					   help='flag to show moneys left in register after dispensing change')

	args = parser.parse_args()
	Register(args.total_cost, args.amount_provided, show_remaining=args.show_remaining)
	print() #newline

if __name__ == '__main__': 
	# 3.6 or higher, assuming 4 will break things so enforcing major version 3 exactly
	if sys.version_info[0] != 3 or sys.version_info[1] < 6:
		raise Exception("Python 3.6 (2016) or a more recent minor version or Python 3 is required.")
	startup()

