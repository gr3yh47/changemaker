import sys
import math

from decimal import Decimal

# this should be a class, but subclassing Decimal was not happening right now
# there are some cpython implementation specific instantiation issues... at 11:30pm .-.
def USD(amount, name='USD'):
	#initial check of correct # of decimal places for USD 
	amount = Decimal(amount)
	if amount.as_tuple().exponent <-2:
		print(f'WARNING: {name} amount has been rounded to whole cents')
		amount = round(amount, 2)

	return amount

class Register(object):
	# USD denominations in our drawer. 
	# This is ripe for extension to supporting other currencies
	drawer = {
		'$100 bill': {'value': '100'},
		'$50 bill': {'value': '50'},
		'$20 bill': {'value': '20'},
		'$10 bill': {'value': '10'},
		'$5 bill': {'value': '5'},
		'$1 bill': {'value': '1'},
		'quarter': {'value': '0.25'},
		'dime': {'value': '0.10'},
		'nickel': {'value': '0.05'},
		'penny': {
			'value': '0.01', 
			'plural': 'pennies',
		},
	}

	change_by_unit = {}

	def __init__(self, total_cost, amount_provided, *args, **kwargs):
		# register is a one-off each time for command line invocation
		# this is a little awkward but for the scope of the challenge it seems appropriate
		self.show_remaining = kwargs.pop('show_remaining', False) #used by output_change
		self.total_cost = USD(total_cost, name='total_cost')
		self.amount_provided = USD(amount_provided, name='amount_provided')
		self.change_due = self.amount_provided - self.total_cost

		if self.change_due < 0:
			money_needed = abs(self.change_due)
			print('\nERROR! The amount provided must be greater than the total cost.')
			print(f'At least ${money_needed:.2f} more must be provided by the customer to complete this purchase')
			print('TAANSTAFL!')
			return

		# by default load 50 of each denomination in register
		# could be improved to handle json defining register load
		for unit in self.drawer.keys():
			self.load_register(unit, 50)

		self.calculate_change()
		self.output_change()

		
	# alter the quantity of a unit of currency in the register
	def load_register(self, unit, quantity, *args, **kwargs):
		self.drawer[unit]['qty'] = quantity

	def calculate_change(self, *args, **kwargs):
		remaining_change_due = self.change_due

		for unit, info in self.drawer.items():
			# whole-number division result to determine quantity of this change unit
			unit_value = USD(info['value'])
			change_qty = math.floor(remaining_change_due / unit_value)

			# a future improvement could check if there's enough left in the drawer
			
			# store quantities of denominations to dispense
			self.change_by_unit[unit] = change_qty

			# calculate remaining chainge due
			amount_dispensed = unit_value * change_qty
			remaining_change_due -= amount_dispensed

	def output_change(self, *args, **kwargs):
		print(f'\nThe purchase price was ${self.total_cost:.2f}')
		print(f'The amount tendered was ${self.amount_provided:.2f}')

		if self.change_due > 0:

			print(f'The total change due is ${self.change_due:.2f}\n')
			print('Please dispense the following units of change:')
			print('Qty \t unit')
			print('--- \t ----')

			for unit, quantity in self.change_by_unit.items():
				#handle plurality
				unit_name = unit #default to singular
				if quantity != 1:
					# plural is unit name with an 's' appended if an explicit plural doesnt exist
					unit_name = self.drawer[unit].get('plural', unit_name + 's')

				#only output a those units of change we actually dispense
				if quantity > 0:
					print(f'({quantity})\t {unit}')
					self.drawer[unit]['qty'] -= quantity
		else:
			print('\nThere is no change due.')
			print('Please thank the customer for providing exact change!!')

		#output remaining bills in register if indicated
		if self.show_remaining:
			print(f'\nregister now contains the following moneys:')
			for unit, info in self.drawer.items():
				qty = info['qty']
				# display_unit = '|'.join(str(x).ljust(12)
				print(f'{unit.ljust(8)}: ({qty})')

