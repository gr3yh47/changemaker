# README #

### 'Register' Code Challenge ###

When executed or loaded it should accept two arguments:
1) "Total cost" (In USD)
2) "Amount provided" (also in USD).  
Return as output the change that should be provided, not in USD, but instead by returning the count of each denomination of bills and/or coins that should be returned as change.

### Architecture ###

I selected Python 3 to match current standards. 
!!! This application requires Python 3.6(2016) or newer due to the use of builtin dict ordering !!!
This was also a nice opportunity to get some more exposure to 3 as my current day-to-day environment is 2.7

I also intentionally only used the python standard library for ease of use/portability

### Design Philosophy ###

I took the 'arguments' part of the challenge quite literally and decided to accept command-line arguments 
(as opposed to i.e. promting for user input)

I started with a basic functional script to get the fundamental logic in place.
This also helped me discover some pitfalls and issues (i.e. floats definitely do not work for currency)

Once I had a satisfactory MVP functional version, I created a new class-based version.
In addition to using a 'Register' class, and overall having less/cleaner code, the class-based version
also has much better handling of arguments using the argparse module. 
It also has much prettier output overall and more robust error handling.

I love code clarity and organization. I have also been a siloed developer for several years.
I welcome and encourage feedback on code readability and understandability. 

### Usage ###

My 'official' submission is the class-based version. for help, run:
python classBasedRegister.py -h

The standard use case takes total_cost and amount_provided as positional arguments, i.e.:
python classBasedRegister.py 150.27 160

If you want to run the legacy functional version, i left it in, the usage is:
python functionalRegister.py 150.27 160

### Improvements ###

There are several clear paths to improving this code:
* Actual error and warning handling in several places instead of simple print messages.
* Persistent tracking of how much money is left in the drawer vs each run starting fresh
* Handling not enough of a given unit of currency (i.e. i need to give $50 change and only have one $20, so i need to do a $20 and 3 $10s)

In spite of these I believe I fulfilled the purpose of this challenge. There are always improvements that can be made.
