import sys
import math

from decimal import Decimal, ROUND_HALF_UP


# default register load assumes USD
DEFAULT = {
	'currency': 'USD',
	'symbol': '$',
	'max_decimals': 2,
	'denominations': [
		{
			'unit_name': '$20 bill',
			'unit_value': 20
		}, {
			'unit_name': '$10 bill',
			'unit_value': 10,
		}, {
			'unit_name': '$5 bill',
			'unit_value': 5,
		}, {
			'unit_name': '$1 bill',
			'unit_value': 1,
		}, {
			'unit_name': 'quarter',
			'unit_value': Decimal('0.25'),
		}, {
			'unit_name': 'dime',
			'unit_value': Decimal('0.10'),
		}, {
			'unit_name': 'nickel',
			'unit_value': Decimal('0.05'),
		}, {
			'unit_name': 'penny',
			'unit_value': Decimal('0.01'),
			'plural': 'pennies'
		},
	]
}

drawer = DEFAULT

# initial tasks + checking for proper usage
def startup():
	# Check for enough arguments
	try: 
		total_cost = Decimal(sys.argv[1])
		amount_provided = Decimal(sys.argv[2])
	except:
		print('please run this with two positional arguments: total_cost and amount_provided')
		print('both arguments must be valid amounts for currency ' + drawer['currency'])
		sys.exit(2)

	# check if amounts have more than 2 decimals and warn about rounding. 
	if any([amount_provided.as_tuple().exponent < -2, total_cost.as_tuple().exponent < -2]):
		print('warning: USD amounts have been rounded to whole cents')
		total_cost = round(total_cost, 2)
		amount_provided = round(amount_provided, 2)

	#check if enough money was provided. 
	if total_cost > amount_provided:
		amount_short = total_cost - amount_provided
		print('not enough money for purchase!')
		print('at least ' + drawer['symbol'] + str(amount_short) + ' more money is required')
		sys.exit(1)

	return get_change(total_cost, amount_provided)

# top-level logic function
def get_change(total_cost, amount_provided):
	# calculate total change due
	change_due = amount_provided - total_cost

	# convenience output for the user
	print(f'the purchase price was: ${total_cost}')
	print(f'the amount provided was: ${amount_provided}')
	print(f'the total change due is: ${change_due}')

	# retrieve specific change quantities
	change_list = calculate_change(change_due)	

	output_change(change_list)

# recursive function that calculates and eventually returns all of the unit quantities of change
def calculate_change(change_due, denomination_index=0, running_change=[]):
	change_unit = drawer['denominations'][denomination_index]
	unit_name = change_unit['unit_name']
	unit_value = change_unit['unit_value']

	# divide and round down to see how many of this unit can be dispensed for change
	unit_qty = math.floor(change_due/unit_value)

	# add the quantity of this denomination of change to our record
	running_change.append(unit_qty)

	# subtract change dispensed to get remaining change due
	change_dispensed = unit_qty * unit_value
	remaining_change = change_due - change_dispensed

	denomination_index += 1

	# if we are at the bottom denomination, return the results, else recurse
	if denomination_index == len(drawer['denominations']):
		return running_change
	else: 
		return calculate_change(remaining_change, denomination_index, running_change)

# user friendly output of results. 
def output_change(change_list):
	denoms = drawer['denominations']
	print('please dispense the following units of change:')
	for qty, unit in zip(change_list, denoms):

		# handle singular and plural currency labels
		if qty == 1:
			display_name = unit['unit_name']
		elif qty > 1:
			#if there is a special plural defined, get it, else append 's'
			display_name = unit.get('plural', unit['unit_name'] + 's')
		else:
			continue # do not output if none of that denomination is required.

		print(f'{qty} {display_name}')

if __name__ == '__main__': 
	print('starting up!')
	startup()
